package com.example.product;

import com.example.position.Position;

public class MapElement {
    private char alias;
    private Position position;

    public MapElement() {
    }

    public char getAlias() {
        return alias;
    }

    public void setAlias(char alias) {
        this.alias = alias;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
