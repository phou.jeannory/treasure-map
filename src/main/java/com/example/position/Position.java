package com.example.position;

public class Position {
    protected int horizontalAxis;//west to east
    protected int verticalAxis;//north to south

    public Position(int horizontalAxis, int verticalAxis) {
        this.horizontalAxis = horizontalAxis;
        this.verticalAxis = verticalAxis;
    }

    public int getHorizontalAxis() {
        return horizontalAxis;
    }

    public void setHorizontalAxis(int horizontalAxis) {
        this.horizontalAxis = horizontalAxis;
    }

    public int getVerticalAxis() {
        return verticalAxis;
    }

    public void setVerticalAxis(int verticalAxis) {
        this.verticalAxis = verticalAxis;
    }

    //[horizontalAxis,verticalAxis]
    @Override
    public String toString() {
        return "[" + horizontalAxis + "," + verticalAxis + "]";
    }
}
