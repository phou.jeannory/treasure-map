package com.example.position;

public class AdventuredPosition extends Position {
    private char orientation;

    public AdventuredPosition(int horizontalAxis, int verticalAxis, char orientation) {
        super(horizontalAxis, verticalAxis);
        this.orientation = orientation;
    }

    public int getHorizontalAxis() {
        return horizontalAxis;
    }

    public void setHorizontalAxis(int horizontalAxis) {
        this.horizontalAxis = horizontalAxis;
    }

    public int getVerticalAxis() {
        return verticalAxis;
    }

    public void setVerticalAxis(int verticalAxis) {
        this.verticalAxis = verticalAxis;
    }

    public char getOrientation() {
        return orientation;
    }

    public void setOrientation(char orientation) {
        this.orientation = orientation;
    }

    //[horizontalAxis,verticalAxis,orientation]
    @Override
    public String toString() {
        return "[" + horizontalAxis + "," + verticalAxis + "," + orientation + "]";
    }
}
