package com.example;

import com.example.components.IMapService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TreasureMapApplication {

    /**
     *
     read the file in resources/input and write result of the treasure hunt (path on application.properties)
     */
    public static void main(String[] args) {
        SpringApplication.run(TreasureMapApplication.class, args);
    }

    @Bean
    CommandLineRunner start(
            final IMapService mapService
    ) {
        return arg -> {
            mapService.writeResult();
        };
    }

}
