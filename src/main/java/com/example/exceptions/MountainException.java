package com.example.exceptions;

public class MountainException extends RuntimeException {

    public MountainException(String message) {
        super(message);
    }
}
