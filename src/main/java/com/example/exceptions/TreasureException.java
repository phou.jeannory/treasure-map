package com.example.exceptions;

public class TreasureException extends RuntimeException {
    public TreasureException(String message) {
        super(message);
    }
}
