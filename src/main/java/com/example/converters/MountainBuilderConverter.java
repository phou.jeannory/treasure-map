package com.example.converters;

import com.example.builders.MapElementBuilder;
import com.example.builders.MountainBuilder;
import com.example.position.Position;
import com.example.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("MountainBuilderConverter")
public class MountainBuilderConverter<B> implements Converter<B> {

    private MapUtils mapUtils;

    @Autowired
    public void setMapUtils(MapUtils mapUtils) {
        this.mapUtils = mapUtils;
    }

    @Override
    public List<B> toMapElementBuilders() {

        List<String> fileStrings = mapUtils.getFileStrings(MountainBuilder.class);

        //remove spaces
        fileStrings = fileStrings.stream().map(file -> file.replaceAll(" ", "")).collect(Collectors.toList());

        //build MapElementBuilder
        final List<MapElementBuilder> mountainBuilders = new ArrayList<>();
        fileStrings.forEach(file -> {
            String[] strings = file.split("-");
            mountainBuilders.add(new MountainBuilder(new Position(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]))));
        });
        return (List<B>) mountainBuilders;
    }

    @Override
    public List<String> toNewString(List<B> mapElementBuilders) {
        return ((List<MountainBuilder>) mapElementBuilders).stream().map(
                mapElementBuilder -> mapElementBuilder.getAlias() + " - "
                        + mapElementBuilder.getPosition().getHorizontalAxis() + " - "
                        + mapElementBuilder.getPosition().getVerticalAxis()
        ).collect(Collectors.toList());
    }

}
