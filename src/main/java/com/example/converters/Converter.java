package com.example.converters;

import com.example.utils.MapUtils;

import java.util.List;

public interface Converter <B>{

    void setMapUtils(MapUtils mapUtils);

    List<B> toMapElementBuilders();

    List<String> toNewString(List<B> mapElementBuilders);

}
