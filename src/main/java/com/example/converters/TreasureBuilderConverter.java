package com.example.converters;

import com.example.builders.MapElementBuilder;
import com.example.builders.TreasureBuilder;
import com.example.position.Position;
import com.example.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component("TreasureBuilderConverter")
public class TreasureBuilderConverter<B> implements Converter<B> {

    private MapUtils mapUtils;

    @Autowired
    public void setMapUtils(MapUtils mapUtils) {
        this.mapUtils = mapUtils;
    }

    @Override
    public List<B> toMapElementBuilders() {

        List<String> fileStrings = mapUtils.getFileStrings(TreasureBuilder.class);

        //remove spaces
        fileStrings = fileStrings.stream().map(file -> file.replaceAll(" ", "")).collect(Collectors.toList());

        //build TreasureBuilder
        final List<MapElementBuilder> treasureBuilders = new ArrayList<>();
        fileStrings.forEach(file -> {
            String[] strings = file.split("-");
            treasureBuilders.add(new TreasureBuilder(new Position(Integer.parseInt(strings[1]), Integer.parseInt(strings[2])), Integer.parseInt(strings[3])));
        });
        return (List<B>) treasureBuilders;
    }

    @Override
    public List<String> toNewString(List<B> mapElementBuilders) {

        final List<TreasureBuilder> treasureBuilders = (List<TreasureBuilder>) mapElementBuilders;
        return treasureBuilders.stream().map(
                mapElementBuilder -> mapElementBuilder.getAlias() + " - "
                        + mapElementBuilder.getPosition().getHorizontalAxis() + " - "
                        + mapElementBuilder.getPosition().getVerticalAxis() + " - "
                        + mapElementBuilder.getTreasureAmount()
        ).collect(Collectors.toList());
    }
}
