package com.example.converters;

import com.example.builders.AdventuredBuilder;
import com.example.builders.MapElementBuilder;
import com.example.position.AdventuredPosition;
import com.example.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component("AdventuredBuilderConverter")
public class AdventuredBuilderConverter<B> implements Converter<B> {

    private MapUtils mapUtils;

    @Autowired
    public void setMapUtils(MapUtils mapUtils) {
        this.mapUtils = mapUtils;
    }

    @Override
    public List<B> toMapElementBuilders() {
        List<String> fileStrings = mapUtils.getFileStrings(AdventuredBuilder.class);

        //remove spaces
        fileStrings = fileStrings.stream().map(file -> file.replaceAll(" ", "")).collect(Collectors.toList());

        //build AdventuredBuilders
        final List<MapElementBuilder> adventuredBuilders = new ArrayList<>();
        fileStrings.forEach(file -> {
            String[] strings = file.split("-");

            //build moves
            final List<Character> moves = strings[5].chars().mapToObj(e -> (char) e).collect(Collectors.toList());

            adventuredBuilders.add(
                    new AdventuredBuilder(
                            new AdventuredPosition(Integer.parseInt(strings[2]), Integer.parseInt(strings[3]), strings[4].charAt(0)),
                            strings[1],
                            moves)
            );
        });
        return (List<B>) adventuredBuilders;
    }

    @Override
    public List<String> toNewString(List<B> mapElementBuilders) {

        final List<AdventuredBuilder> adventuredBuilder = (List<AdventuredBuilder>) mapElementBuilders;
        return adventuredBuilder.stream().map(
                mapElementBuilder -> mapElementBuilder.getAlias() + " - "
                        + mapElementBuilder.getName() + " - "
                        + mapElementBuilder.getPosition().getHorizontalAxis() + " - "
                        + mapElementBuilder.getPosition().getVerticalAxis() + " - "
                        + ((AdventuredPosition)mapElementBuilder.getPosition()).getOrientation() + " - "
                        + mapElementBuilder.getTreasureAmount()
        ).collect(Collectors.toList());
    }

}
