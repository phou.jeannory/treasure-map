package com.example.converters;

import com.example.director.MapDirector;
import com.example.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class MapDirectorConverter {
    private MapUtils mapUtils;

    @Autowired
    public void setMapUtils(MapUtils mapUtils) {
        this.mapUtils = mapUtils;
    }

    public MapDirector toMapDirector() {

        final List<String> fileStrings = mapUtils.getFileStrings(MapDirector.class);

        //only one mapDirector on input file
        //remove spaces
        final String str = fileStrings.get(0).replaceAll(" ", "");

        final String[] strings = str.split("-");

        return new MapDirector(Integer.parseInt(strings[1]), Integer.parseInt(strings[2]));

    }

    public String toNewString(MapDirector mapDirector) {
        return mapDirector.getAlias() + " - " + mapDirector.getWidth() + " - " + mapDirector.getHeight();
    }
}
