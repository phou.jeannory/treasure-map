package com.example.builders;

import com.example.position.Position;
import com.example.product.MapElement;

import static com.example.constants.Constants.MOUNTAIN_CHAR;

public class MountainBuilder extends MapElementBuilder {

    public MountainBuilder(Position position) {
        mapElement = new MapElement();
        mapElement.setAlias(MOUNTAIN_CHAR);
        mapElement.setPosition(position);
    }

    @Override
    public char getAlias() {
        return mapElement.getAlias();
    }

    @Override
    public Position getPosition() {
        return mapElement.getPosition();
    }

    @Override
    public void setPosition(Position position) {
        mapElement.setPosition(position);
    }

}
