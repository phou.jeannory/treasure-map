package com.example.builders;

import com.example.position.Position;
import com.example.product.MapElement;

import static com.example.constants.Constants.TREASURE_CHAR;

public class TreasureBuilder extends MapElementBuilder {

    private int treasureAmount;

    public TreasureBuilder(Position position, int treasureAmount) {
        mapElement = new MapElement();
        mapElement.setAlias(TREASURE_CHAR);
        mapElement.setPosition(position);
        this.treasureAmount = treasureAmount;
    }

    @Override
    public char getAlias() {
        return mapElement.getAlias();
    }

    @Override
    public Position getPosition() {
        return mapElement.getPosition();
    }

    @Override
    public void setPosition(Position position) {
        mapElement.setPosition(position);
    }

    public int getTreasureAmount() {
        return treasureAmount;
    }

    public void setTreasureAmount(int treasureAmount) {
        this.treasureAmount = treasureAmount;
    }
}
