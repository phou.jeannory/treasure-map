package com.example.builders;

import com.example.position.Position;
import com.example.product.MapElement;

public abstract class MapElementBuilder {
    protected MapElement mapElement;

    public abstract char getAlias();

    public abstract Position getPosition();

    public abstract void setPosition(Position position);
}
