package com.example.builders;


import com.example.position.AdventuredPosition;
import com.example.position.Position;
import com.example.product.MapElement;

import java.util.List;

import static com.example.constants.Constants.ADVENTURED_CHAR;

public class AdventuredBuilder extends MapElementBuilder {

    private String name;
    private int treasureAmount;
    private List<Character> moves;

    public AdventuredBuilder(AdventuredPosition adventuredPosition, String name, List<Character> moves) {
        mapElement = new MapElement();
        mapElement.setAlias(ADVENTURED_CHAR);
        mapElement.setPosition(adventuredPosition);
        this.name = name;
        this.moves = moves;
    }

    public char getAlias() {
        return mapElement.getAlias();
    }

    public Position getPosition() {
        return mapElement.getPosition();
    }

    public void setPosition(Position position) {
        mapElement.setPosition(position);
    }

    public String getName() {
        return name;
    }

    public int getTreasureAmount() {
        return treasureAmount;
    }

    public void setTreasureAmount(int treasureAmount) {
        this.treasureAmount = treasureAmount;
    }

    public List<Character> getMoves() {
        return moves;
    }

}
