package com.example.director;

import com.example.builders.AdventuredBuilder;
import com.example.builders.MountainBuilder;
import com.example.builders.TreasureBuilder;
import com.example.exceptions.MapException;
import com.example.exceptions.MountainException;
import com.example.exceptions.TreasureException;
import com.example.position.AdventuredPosition;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static com.example.constants.Constants.MAP_CHAR;

public class MapDirector {
    private final char ALIAS;
    private int width; //x start at 0
    private int height;//y start at 0
    private List<MountainBuilder> mountainBuilders;
    private List<TreasureBuilder> treasureBuilders;
    private List<AdventuredBuilder> adventuredBuilders;

    public MapDirector(int width, int height) {
        ALIAS = MAP_CHAR;
        this.width = width;
        this.height = height;
    }

    public char getAlias() {
        return ALIAS;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<MountainBuilder> getMountainBuilders() {
        return mountainBuilders;
    }

    public void setMountainBuilders(List<MountainBuilder> mountainBuilders) {
        this.mountainBuilders = mountainBuilders;
    }

    public List<TreasureBuilder> getTreasureBuilders() {
        return treasureBuilders.stream().filter(treasureBuilder -> treasureBuilder.getTreasureAmount() >0).collect(Collectors.toList());
    }

    public void setTreasureBuilders(List<TreasureBuilder> treasureBuilders) {
        this.treasureBuilders = treasureBuilders;
    }

    public List<AdventuredBuilder> getAdventuredBuilders() {
        return adventuredBuilders;
    }

    public void setAdventuredBuilders(List<AdventuredBuilder> adventuredBuilders) {
        this.adventuredBuilders = adventuredBuilders;
    }

    public void adventuredMoves() {
        int i = 0;

        //loop on all moves
        while (i < getMaxMoves()) {
            int j = 0;

            //loop on all adventureds
            while (j < adventuredBuilders.size()) {

                //check ArrayIndexOutOfBoundsException before next steps
                //only if next steps index <= adventured(j).move.size
                if (!(i >= getMaxMoves(adventuredBuilders.get(j)))) {

                    final char move = adventuredBuilders.get(j).getMoves().get(i);

                    switch (move) {
                        case 'A':
                            adventuredBuilders.get(j).setPosition(moveForward((AdventuredPosition) adventuredBuilders.get(j).getPosition()));
                            if (findTreasure(adventuredBuilders.get(j))) {
                                System.out.println("adventured find a treasure");
                            }
                            break;
                        case 'G':
                            adventuredBuilders.get(j).setPosition(toTheLeft((AdventuredPosition) adventuredBuilders.get(j).getPosition()));
                            break;
                        case 'D':
                            adventuredBuilders.get(j).setPosition(toTheRight((AdventuredPosition) adventuredBuilders.get(j).getPosition()));
                            break;
                    }
                }
                j += 1;
            }
            i += 1;
        }
    }

    public int getMaxMoves() {
        //handle NullPointerException
        if (null == adventuredBuilders) {
            return 0;
        }
        return adventuredBuilders.stream().map(
                adventuredBuilder -> adventuredBuilder.getMoves().size()).max(Comparator.comparing(Integer::intValue)).orElse(0);
    }

    public int getMaxMoves(AdventuredBuilder adventuredBuilder) {
        return adventuredBuilder.getMoves().size();
    }

    private boolean isValidMove(AdventuredPosition adventuredPosition) {
        //handle nullPointerException
        if (null == this.mountainBuilders) {
            System.out.println("no mountain in this map");
        } else {
            this.mountainBuilders
                    .stream().map(MountainBuilder::getPosition).collect(Collectors.toList())
                    .forEach(position -> {
                        if (
                                position.getHorizontalAxis() == adventuredPosition.getHorizontalAxis()
                                        && position.getVerticalAxis() == adventuredPosition.getVerticalAxis()
                        ) {
                            throw new MountainException("Adventured cannot cross mountain");
                        }
                    });
        }


        if (
                adventuredPosition.getVerticalAxis() >= this.height ||
                        adventuredPosition.getVerticalAxis() < 0 ||
                        adventuredPosition.getHorizontalAxis() >= this.width ||
                        adventuredPosition.getHorizontalAxis() < 0
        ) {
            throw new MapException("Adventured cannot go outside the map");
        }
        return true;
    }

    private boolean findTreasure(AdventuredBuilder adventuredBuilder) {
        //handle nullPointerException
        if (null == this.treasureBuilders) {
            return false;
        }
        final AtomicBoolean result = new AtomicBoolean(false);
        this.treasureBuilders.forEach(
                treasureBuilder -> {
                    if (
                            treasureBuilder.getPosition().getHorizontalAxis() == adventuredBuilder.getPosition().getHorizontalAxis()
                                    && treasureBuilder.getPosition().getVerticalAxis() == adventuredBuilder.getPosition().getVerticalAxis()
                    ) {
                        try {
                            if (isEnoughTreasureAmount(treasureBuilder)) {
                                treasureBuilder.setTreasureAmount(treasureBuilder.getTreasureAmount() - 1);
                                adventuredBuilder.setTreasureAmount(adventuredBuilder.getTreasureAmount() + 1);
                                result.set(true);
                            }
                        } catch (TreasureException ex) {
                            System.out.println(ex.getMessage());
                        }

                    }
                });
        return result.get();
    }

    private boolean isEnoughTreasureAmount(TreasureBuilder treasureBuilder) {
        if (treasureBuilder.getTreasureAmount() < 1) {
            throw new TreasureException("no more treasure here!");
        }
        return true;
    }

    //return whole AdventuredPosition
    //A
    private AdventuredPosition moveForward(AdventuredPosition adventuredPosition) {
        final AdventuredPosition newAdventuredPosition = new AdventuredPosition(
                adventuredPosition.getHorizontalAxis(),
                adventuredPosition.getVerticalAxis(),
                adventuredPosition.getOrientation()
        );
        switch (adventuredPosition.getOrientation()) {
            case 'S':
                newAdventuredPosition.setVerticalAxis(adventuredPosition.getVerticalAxis() + 1);
                break;
            case 'E':
                newAdventuredPosition.setHorizontalAxis(adventuredPosition.getHorizontalAxis() + 1);
                break;
            case 'N':
                newAdventuredPosition.setVerticalAxis(adventuredPosition.getVerticalAxis() - 1);
                break;
            case 'O':
                newAdventuredPosition.setHorizontalAxis(adventuredPosition.getHorizontalAxis() - 1);
                break;
        }
        try {
            if (isValidMove(newAdventuredPosition)) {
                System.out.println("Adventured can move forward");
                return newAdventuredPosition;
            }
        } catch (MountainException | MapException ex) {
            System.out.println(ex.getMessage());
        }

        return adventuredPosition;
    }

    //G
    private AdventuredPosition toTheLeft(AdventuredPosition adventuredPosition) {
        switch (adventuredPosition.getOrientation()) {
            case 'S':
                adventuredPosition.setOrientation('E');
                break;
            case 'E':
                adventuredPosition.setOrientation('N');
                break;
            case 'N':
                adventuredPosition.setOrientation('O');
                break;
            case 'O':
                adventuredPosition.setOrientation('S');
                break;
        }
        return adventuredPosition;
    }

    //D
    private AdventuredPosition toTheRight(AdventuredPosition adventuredPosition) {
        switch (adventuredPosition.getOrientation()) {
            case 'S':
                adventuredPosition.setOrientation('O');
                break;
            case 'O':
                adventuredPosition.setOrientation('N');
                break;
            case 'N':
                adventuredPosition.setOrientation('E');
                break;
            case 'E':
                adventuredPosition.setOrientation('S');
                break;
        }
        return adventuredPosition;
    }

}
