package com.example.utils;

import com.example.TreasureMapApplication;
import com.example.builders.AdventuredBuilder;
import com.example.builders.MountainBuilder;
import com.example.builders.TreasureBuilder;
import com.example.director.MapDirector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MapUtils {

    private Environment env;

    @Autowired
    public void setEnv(Environment env) {
        this.env = env;
    }

    public List<String> toComments() {
        return getFileStrings().stream().filter(line -> line.startsWith("#")).collect(Collectors.toList());
    }

    public List<String> getFileStrings(Class aClass) {
        if (aClass == MapDirector.class) {
            return Objects.requireNonNull(getFileStrings()).stream().filter(line -> line.startsWith("C")).collect(Collectors.toList());
        } else if (aClass == MountainBuilder.class) {
            return Objects.requireNonNull(getFileStrings()).stream().filter(line -> line.startsWith("M")).collect(Collectors.toList());
        } else if (aClass == TreasureBuilder.class) {
            return Objects.requireNonNull(getFileStrings()).stream().filter(line -> line.startsWith("T")).collect(Collectors.toList());
        } else if (aClass == AdventuredBuilder.class) {
            return Objects.requireNonNull(getFileStrings()).stream().filter(line -> line.startsWith("A")).collect(Collectors.toList());
        }
        return null;
    }

    public String getTreasureComments(){
        return "# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}";
    }

    public String getAdventuredComment(){
        return "# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}";
    }

    private List<String> getFileStrings() {
        final List<String> filePaths = getFilePaths();
        final String filePath = filePaths.get(0);
        try (
                Stream<String> stream = Files.lines(Paths.get(filePath))) {
            List<String> strings = stream.collect(Collectors.toList());
            return strings;
        } catch (IOException ex) {
            return null;
        }
    }

    private List<String> getFilePaths() {
        final File folder = new File(TreasureMapApplication.class.getResource("/input").getPath());
        return Arrays.stream(Objects.requireNonNull(folder.listFiles())).map(File::getPath).collect(Collectors.toList());

    }

}
