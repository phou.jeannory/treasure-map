package com.example.constants;

public class Constants {
    public final static char MAP_CHAR = 'C';
    public final static char MOUNTAIN_CHAR = 'M';
    public final static char TREASURE_CHAR = 'T';
    public final static char ADVENTURED_CHAR = 'A';
}
