package com.example.components;

import com.example.TreasureMapApplication;
import com.example.builders.AdventuredBuilder;
import com.example.builders.MapElementBuilder;
import com.example.builders.MountainBuilder;
import com.example.builders.TreasureBuilder;
import com.example.converters.*;
import com.example.director.MapDirector;
import com.example.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Component
public class MapServiceImpl implements IMapService {

    private MapDirectorConverter mapDirectorConverter;
    private Converter<MountainBuilder> mountainBuilderConverter;
    private Converter<TreasureBuilder> treasureBuilderConverter;
    private Converter<AdventuredBuilder> adventuredBuilderConverter;
    private Environment env;
    private MapUtils mapUtils;

    @Autowired
    public void setMapDirectorConverter(MapDirectorConverter mapDirectorConverter) {
        this.mapDirectorConverter = mapDirectorConverter;
    }

    @Autowired
    @Qualifier("MountainBuilderConverter")
    public void setMountainBuilderConverter(Converter mountainBuilderConverter) {
        this.mountainBuilderConverter = mountainBuilderConverter;
    }
    @Autowired
    @Qualifier("TreasureBuilderConverter")
    public void setTreasureBuilderConverter(Converter treasureBuilderConverter) {
        this.treasureBuilderConverter = treasureBuilderConverter;
    }
    @Autowired
    @Qualifier("AdventuredBuilderConverter")
    public void setAdventuredBuilderConverter(Converter adventuredBuilderConverter) {
        this.adventuredBuilderConverter = adventuredBuilderConverter;
    }
    @Autowired
    public void setEnv(Environment env) {
        this.env = env;
    }
    @Autowired
    public void setMapUtils(MapUtils mapUtils) {
        this.mapUtils = mapUtils;
    }

    @Override
    public void writeResult() throws IOException {
        //build map
        final MapDirector mapDirector = mapDirectorConverter.toMapDirector();

        //build mountain
        final List<MountainBuilder> mountainBuilders = mountainBuilderConverter.toMapElementBuilders();

        //build treasure
        final List<TreasureBuilder> treasureBuilders = treasureBuilderConverter.toMapElementBuilders();

        //build adventured
        final List<AdventuredBuilder> adventuredBuilders = adventuredBuilderConverter.toMapElementBuilders();

        mapDirector.setMountainBuilders(mountainBuilders);
        mapDirector.setTreasureBuilders(treasureBuilders);
        mapDirector.setAdventuredBuilders(adventuredBuilders);

        //build all objects
        mapDirector.adventuredMoves();

        //get map
        final String mapString = mapDirectorConverter.toNewString(mapDirector);

        //get mountains
        final List<MountainBuilder> mountainBuilders2 = mapDirector.getMountainBuilders();
        final List<String> mountainStrings = mountainBuilderConverter.toNewString(mountainBuilders2);

        //get treasures
        final String treasureComment = mapUtils.getTreasureComments();
        final List<TreasureBuilder> treasureBuilders2 = mapDirector.getTreasureBuilders();
        final List<String> treasureStrings = treasureBuilderConverter.toNewString(treasureBuilders2);

        //get adventureds
        final String adventuredComment = mapUtils.getAdventuredComment();
        final List<AdventuredBuilder> adventuredBuilders2 = mapDirector.getAdventuredBuilders();
        final List<String> adventuredStrings = adventuredBuilderConverter.toNewString(adventuredBuilders2);

        final String output = env.getProperty("output" , String.class, "");
        final FileWriter writer = new FileWriter(output);

        //write map
        writer.write(mapString + System.lineSeparator());

        //write mountains
        for (String str : mountainStrings) {
            writer.write(str + System.lineSeparator());
        }
        //write treasure
        writer.write(treasureComment + System.lineSeparator());
        for (String str : treasureStrings) {
            writer.write(str + System.lineSeparator());
        }
        //write adventured
        writer.write(adventuredComment + System.lineSeparator());
        for (String str : adventuredStrings) {
            writer.write(str + System.lineSeparator());
        }
        writer.close();

    }

}
