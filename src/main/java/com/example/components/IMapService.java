package com.example.components;

import java.io.IOException;

public interface IMapService {
    void writeResult() throws IOException;
}
