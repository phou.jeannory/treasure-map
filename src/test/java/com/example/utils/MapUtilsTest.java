package com.example.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class MapUtilsTest {

    @Test
    public void getComments() {
        //given
        final MapUtils mapUtils = new MapUtils();

        //when
        final List<String> comments = mapUtils.toComments();

        //then
        Assert.assertEquals(
                "# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}",
                comments.get(0)
        );
        Assert.assertEquals(
                "# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}",
                comments.get(1)
        );
    }
}