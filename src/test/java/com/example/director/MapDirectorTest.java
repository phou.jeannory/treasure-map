package com.example.director;

import com.example.builders.AdventuredBuilder;
import com.example.builders.Builder;
import com.example.builders.TreasureBuilder;
import com.example.position.AdventuredPosition;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MapDirectorTest {

    private MapDirector mapDirector;

    @BeforeEach
    public void setUp() {
        mapDirector = new MapDirector(3, 4);
    }

    @Test
    public void adventuredMovesShouldReturnZeroThreeSouth() {
        //given
        mapDirector.setAdventuredBuilders(Builder.getAdventureds());

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[0,3,S]"
                , adventuredBuilders.get(0).getPosition().toString()
        );
    }

    @Test
    public void adventuredMovesShouldReturnTwoZeroNorth() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('D', 'A', 'G', 'G', 'G', 'A', 'D', 'A', 'A', 'G'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> results = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[2,0,N]"
                , results.get(0).getPosition().toString()
        );
    }

    @Test
    public void adventuredMovesWhenFindMountainsThenTrowExceptionAndShouldReturnZeroOneEast() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('G', 'A', 'G', 'A', 'G', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));
        mapDirector.setMountainBuilders(Builder.getMountains());

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> results = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[0,1,O]"
                , results.get(0).getPosition().toString()
        );
    }

    @Test
    public void adventuredMovesWhenFindTreasureShouldReturnOneThreeSouthWithOneTreasure() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('A', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));
        mapDirector.setTreasureBuilders(Builder.getTreasures());

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> results = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[1,3,S]"
                , results.get(0).getPosition().toString()
        );
        Assert.assertEquals(1, results.get(0).getTreasureAmount());
    }

    @Test
    public void adventuredMovesWhenFindTreasureTreasureShouldReturnTwo() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('A', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));
        mapDirector.setTreasureBuilders(Builder.getTreasures());

        //when
        mapDirector.adventuredMoves();
        final List<TreasureBuilder> results = mapDirector.getTreasureBuilders();

        //then
        Assert.assertEquals(2, results.get(1).getTreasureAmount());
    }

    @Test
    public void adventuredMovesWhenFindAllTreasureShouldReturnEmptyTreasureAndAdventuredFindFiveTreasure() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('A', 'A', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));
        mapDirector.setTreasureBuilders(Builder.getTreasures());

        //when
        mapDirector.adventuredMoves();
        final List<TreasureBuilder> treasureBuilders = mapDirector.getTreasureBuilders();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(Collections.emptyList(), treasureBuilders);
        Assert.assertEquals(5, adventuredBuilders.get(0).getTreasureAmount());
    }

    @Test
    public void adventuredMovesWhenFindAllTreasureShouldThrowExceptionWhenMoveAgainOnTreasure() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('A', 'A', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));
        mapDirector.setTreasureBuilders(Builder.getTreasures());

        //when
        mapDirector.adventuredMoves();
        final List<TreasureBuilder> treasureBuilders = mapDirector.getTreasureBuilders();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(Collections.emptyList(), treasureBuilders);
        Assert.assertEquals(5, adventuredBuilders.get(0).getTreasureAmount());
    }

    @Test
    public void adventuredMovesThrowMapExceptionWhenAdventuredTryGoOutSideOnSouthShoudReturnOneThreeSouth() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('A', 'A', 'A', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[1,3,S]"
                , adventuredBuilders.get(0).getPosition().toString()
        );
    }

    @Test
    public void adventuredMovesThrowMapExceptionWhenAdventuredTryGoOutSideOnWestShoudReturnZeroOneSouth() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('D', 'A', 'A', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[0,1,O]"
                , adventuredBuilders.get(0).getPosition().toString()
        );
    }

    @Test
    public void adventuredMovesThrowMapExceptionWhenAdventuredTryGoOutSideOnNorthShoudReturnOneZeroNorth() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('D', 'D', 'A', 'A', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[1,0,N]"
                , adventuredBuilders.get(0).getPosition().toString()
        );
    }

    @Test
    public void adventuredMovesThrowMapExceptionWhenAdventuredTryGoOutSideOnEastShouldReturnTwoOneEast() {
        //given
        final AdventuredBuilder adventuredBuilder = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder.getMoves()).thenReturn(Arrays.asList('G', 'A', 'A', 'A', 'A'));
        mapDirector.setAdventuredBuilders(Collections.singletonList(adventuredBuilder));

        //when
        mapDirector.adventuredMoves();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(
                "[2,1,E]"
                , adventuredBuilders.get(0).getPosition().toString()
        );
    }

    @Test
    public void getMaxMovesShouldReturnTwentyTwo() {
        //given
        final AdventuredBuilder adventuredBuilder1 = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder1.getMoves()).thenReturn(Arrays.asList('A', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'A'));
        final AdventuredBuilder adventuredBuilder2 = Mockito.spy(new AdventuredBuilder(
                new AdventuredPosition(0, 2, 'S'),
                "thomas",
                Arrays.asList('A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A')
        ));
        mapDirector.setAdventuredBuilders(Arrays.asList(adventuredBuilder1, adventuredBuilder2));

        //when
        final int result = mapDirector.getMaxMoves();

        //then
        Assert.assertEquals(22, result);
    }

    @Test
    public void adventuredMovesWhenTwoAdventuredsFindAllTreasureOneShouldGetThreeTreasureAndTwoForTheSecond() {
        //given
        final AdventuredBuilder adventuredBuilder1 = Mockito.spy(Builder.getAdventureds().get(0));
        Mockito.when(adventuredBuilder1.getMoves()).thenReturn(
                Arrays.asList(
                        'A', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'A'
                )
        );
        final AdventuredBuilder adventuredBuilder2 = Mockito.spy(
                new AdventuredBuilder(
                    new AdventuredPosition(0, 2, 'S'),
                    "thomas",
                    Arrays.asList('A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A', 'D', 'D', 'A')
                )
        );

        mapDirector.setAdventuredBuilders(Arrays.asList(adventuredBuilder1, adventuredBuilder2));
        mapDirector.setTreasureBuilders(Builder.getTreasures());

        //when
        mapDirector.adventuredMoves();
        final List<TreasureBuilder> treasureBuilders = mapDirector.getTreasureBuilders();
        final List<AdventuredBuilder> adventuredBuilders = mapDirector.getAdventuredBuilders();

        //then
        Assert.assertEquals(Collections.emptyList(), treasureBuilders);
        Assert.assertEquals(3, adventuredBuilders.get(0).getTreasureAmount());
        Assert.assertEquals(2, adventuredBuilders.get(1).getTreasureAmount());
    }
}