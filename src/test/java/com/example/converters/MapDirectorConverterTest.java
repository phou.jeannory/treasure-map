package com.example.converters;

import com.example.director.MapDirector;
import com.example.utils.MapUtils;
import org.junit.Assert;
import org.junit.Test;

public class MapDirectorConverterTest {

    @Test
    public void setMapUtils() {
        //given
        final MapDirectorConverter converter = new MapDirectorConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);

        //when
        final MapDirector mapDirector = converter.toMapDirector();

        //then
        Assert.assertEquals(3,mapDirector.getWidth());
        Assert.assertEquals(4,mapDirector.getHeight());
    }

    @Test
    public void toNewString(){
        //given
        final MapDirectorConverter converter = new MapDirectorConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);
        final MapDirector mapDirector = converter.toMapDirector();

        //when
        final String result = converter.toNewString(mapDirector);

        //then
        Assert.assertEquals("C - 3 - 4", result);

    }
}