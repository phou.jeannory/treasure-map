package com.example.converters;

import com.example.builders.AdventuredBuilder;
import com.example.builders.MapElementBuilder;
import com.example.position.AdventuredPosition;
import com.example.utils.MapUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class AdventuredBuilderConverterTest {


    @Test
    public void toMapElementBuilders() {
        //given
        final Converter converter = new AdventuredBuilderConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);

        //when
        final List<MapElementBuilder> adventuredBuilders = converter.toMapElementBuilders();

        //then
        Assert.assertEquals(new AdventuredPosition(1,1, 'S').toString(), adventuredBuilders.get(0).getPosition().toString());
        Assert.assertEquals("Lara",((AdventuredBuilder)adventuredBuilders.get(0)).getName());
        Assert.assertEquals(Arrays.asList('A','A','D','A','D','A','G','G','A'),((AdventuredBuilder)adventuredBuilders.get(0)).getMoves());
    }

    @Test
    public void toNewString() {
        //given
        final Converter converter = new AdventuredBuilderConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);
        final List<MapElementBuilder> adventuredBuilders = converter.toMapElementBuilders();


        //when
        final List<String> results = converter.toNewString(adventuredBuilders);

        //then
        Assert.assertEquals("A - Lara - 1 - 1 - S - 0", results.get(0));

    }
}