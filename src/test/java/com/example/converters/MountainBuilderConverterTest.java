package com.example.converters;

import com.example.builders.MapElementBuilder;
import com.example.position.Position;
import com.example.utils.MapUtils;
import org.junit.Assert;
import org.junit.Test;


import java.util.List;

public class MountainBuilderConverterTest {


    @Test
    public void toMapElementBuilders() {
        //given
        final Converter converter = new MountainBuilderConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);

        //when
        final List<MapElementBuilder> mountainBuilders = converter.toMapElementBuilders();

        //then
        Assert.assertEquals(new Position(1,0).toString(), mountainBuilders.get(0).getPosition().toString());
        Assert.assertEquals(new Position(2,1).toString(), mountainBuilders.get(1).getPosition().toString());
    }

    @Test
    public void toNewString(){
        //given
        final Converter converter = new MountainBuilderConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);
        final List<MapElementBuilder> mountainBuilders = converter.toMapElementBuilders();

        //when
        List<String> results = converter.toNewString(mountainBuilders);

        //then
        Assert.assertEquals("M - 1 - 0", results.get(0));
        Assert.assertEquals("M - 2 - 1", results.get(1));
    }

}