package com.example.converters;

import com.example.builders.MapElementBuilder;
import com.example.builders.TreasureBuilder;
import com.example.position.Position;
import com.example.utils.MapUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TreasureBuilderConverterTest {

    @Test
    public void toMapElementBuilders() {
        //given
        final Converter converter = new TreasureBuilderConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);

        //when
        final List<MapElementBuilder> treasureBuilders = converter.toMapElementBuilders();

        //then
        Assert.assertEquals(new Position(0,3).toString(), treasureBuilders.get(0).getPosition().toString());
        Assert.assertEquals(new Position(1,3).toString(), treasureBuilders.get(1).getPosition().toString());
        Assert.assertEquals(2, ((TreasureBuilder)treasureBuilders.get(0)).getTreasureAmount());
        Assert.assertEquals(3, ((TreasureBuilder)treasureBuilders.get(1)).getTreasureAmount());
    }

    @Test
    public void toNewString(){
        //given
        final Converter converter = new TreasureBuilderConverter();
        final MapUtils mapUtils = new MapUtils();
        converter.setMapUtils(mapUtils);
        final List<MapElementBuilder> treasureBuilder = converter.toMapElementBuilders();

        //when
        final List<String> results = converter.toNewString(treasureBuilder);

        //then
        Assert.assertEquals("T - 0 - 3 - 2", results.get(0));
        Assert.assertEquals("T - 1 - 3 - 3", results.get(1));
    }
}