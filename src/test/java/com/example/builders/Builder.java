package com.example.builders;

import com.example.position.AdventuredPosition;
import com.example.position.Position;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Builder {

    public static List<MountainBuilder> getMountains() {
        return Arrays.asList(
                new MountainBuilder(new Position(1, 0)),
                new MountainBuilder(new Position(2, 1))
        );
    }

    public static List<TreasureBuilder> getTreasures() {
        return Arrays.asList(
                new TreasureBuilder(new Position(0, 3), 2),
                new TreasureBuilder(new Position(1, 3), 3)
        );
    }

    public static List<AdventuredBuilder> getAdventureds() {
        return Collections.singletonList(
                new AdventuredBuilder(
                        new AdventuredPosition(1, 1, 'S'),
                        "Lara",
                        Arrays.asList('A', 'A', 'D', 'A', 'D', 'A', 'G', 'G', 'A')
                )
        );
    }
}
